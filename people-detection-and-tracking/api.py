import time
from datetime import datetime
import json


class Api:

    def __init__(self, name="api.json"):
        self.file = name
        json_file = open(name, "r")
        if not json_file.read(0):
            self.data = {
                "height": None,
                "width": None,
                "data": {}
            }
        else:
            self.data = json.load(json_file)

    def send(self, id, x, y, height, width):
        json_file = open(self.file, "w")
        self.data["height"] = height
        self.data["width"] = width
        time = datetime.now()
        object_data = {
            'time': str(time),
            'x': x,
            'y': y
        }

        if self.data["data"].get(str(id), None) is None:
            self.data["data"][str(id)] = []

        self.data["data"][str(id)].append(object_data)

        json.dump(self.data, json_file)
